var VUE_APP = new Vue({
    el: "#app",
    data:{
    	loaded: false,
   		one: 1,
   		two: 1,
        showSidebar: false,
        showOverlay: false
    },
    created:function(){
    	setTimeout(this.onLoad,1000);
    },
    methods:{
        onLoad:function(){
            this.loaded = true;
        },
        toggleSidebar:function(){
            this.showSidebar = !this.showSidebar;
            this.showOverlay = !this.showOverlay;
        },
        onClickOverlay:function(){
            this.showOverlay = false;
            this.showSidebar = false;
        }
    }
});